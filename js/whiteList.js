var currentTab = 0;
showTab(currentTab);
function validateForm() {
  var x, y, valid = false;
  x= document.getElementsByClassName('formset')
  y = x[currentTab].getElementsByTagName('input')
  for (let i = 0; i < y.length; i++) {
    if(y[i].checked === true)
      valid = true
    if(y[i].type === 'email'){
      var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
      if(y[i].value.match(mailformat))
        valid = true
        else {
          y[i].classList.add('warning')
          valid = false
        }
    }
    if (y[i].value == "") {
      y[i].className += " invalid";
      valid = false;
    }
  }
  return valid
}
function showTab(n) {
  var x = document.getElementsByClassName('formset')
  var xHeading = document.getElementsByClassName('title')
  var step = document.getElementsByClassName('step')
  x[n].style.display = 'block';
  xHeading[n].style.display = 'block';
  step[n].classList.add("active")
}

function nextPrev(n) {
  var x = document.getElementsByClassName('formset')
  var xHeading = document.getElementsByClassName('title')
  var step = document.getElementsByClassName('step')
  if (n == 1 && !validateForm()) return false;
  x[currentTab].style.display = 'none';
  xHeading[currentTab].style.display = 'none';
  step[currentTab].classList.remove('active')
  currentTab += n;
  if (currentTab >= x.length) {
    document.getElementById('form-container').submit()
    return false
  }
  showTab(currentTab);
}

function displayField(){
  var x= document.getElementsByClassName('formset')
  var y = x[1].getElementsByTagName('input')
  for( let i = 0; i < y.length; i++){
    if(y[i].checked === true){
      document.getElementById(y[i].id + '-field').style.display = 'block'
    } else {
      document.getElementById(y[i].id + '-field').style.display = 'none'
    }
  }
}

const submitData = () => {
  const loader = document.getElementById('spinner');
  loader.style.display = "inline-block"
  const email = $('#email').val()
  const BTC = document.getElementById('BTC-field').children[1].value
  const ETH = document.getElementById('ETH-field').children[1].value
  const ERC20Tether = document.getElementById('Tether-field').children[1].value
  const ERC20USDC = document.getElementById('USDC-field').children[1].value
  let resStatus = ''
  fetch('https://project-artha-backend.stg.qume.io/api/user/create', {
    method: 'POST',
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
      "email": email,
      "BTC": BTC,
      "ETH": ETH,
      "ERC20Tether": ERC20Tether,
      "ERC20USDC": ERC20USDC,
      })
  })
  .then( response => {
    resStatus = response.status
    if(response.status){
      loader.style.display = "none"
    }
    if(response.status == 404 || response.status == 400 || response.status == 500){
      modalShow('Something went wrong. Please try later')
    }
    return response.json()
  })
  .then( data => {
    localStorage.setItem('whitelistEmail', data.email);
    if(data.kyc_status == null){
      if(data.flag === 1 && resStatus === 409){
        modalShow('Data already exists. Need to complete KYC process, Now redirecting to KYC')
        setTimeout(sendtoKYC, 3000)
        setTimeout(reload, 5000);
      }
      else if(resStatus ===201){
        modalShow('Your Data is submitted Successfully. Now Redirecting to Fractal KYC')
        setTimeout(sendtoKYC, 3000)
        setTimeout(reload, 5000);
      }
    }else if(data.kyc_status !== null && data.flag === 1){
      modalShow('Data already exists. You have also completed your KYC process')
      setTimeout(reload, 10000);
    }
  })
  .catch( error => {
    modalShow('Cannot save the data');
    loader.style.display = "none"
  })
  }

function sendtoKYC(){
  window.open("https://fractal.id/authorize?client_id=esJ6bfFr_rbguT1kGCqBMKLeepFTxl6OvK6PnGNhCJY&redirect_uri=https%3A%2F%2Fwww.projectartha.com%2F&response_type=code&scope=contact%3Aread%20verification.basic%3Aread%20verification.basic.details%3Aread%20verification.liveness%3Aread%20verification.liveness.details%3Aread","_blank")
}
function reload(){
  location.reload()
}

function modalShow(text){
  var modal = document.getElementById('myModal1')
  document.getElementById('text-msg').innerHTML = text
  modal.style.display = 'block'
}
const handleClose = (event) => {
  event.target.parentNode.parentNode.parentNode.style.display = 'none'
}