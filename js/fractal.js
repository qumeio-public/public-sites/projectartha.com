getQueryStringParams = query => {
    return query
      ? (/^[?#]/.test(query) ? query.slice(1) : query)
        .split('&')
        .reduce((params, param) => {
            let [key, value] = param.split('=');
            params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
            return params;
          }, {}
        )
      : {}
  };
  let kyc = getQueryStringParams(location.search)
  if(kyc.code){
    let cachedEmail = localStorage.getItem('whitelistEmail') 
    fetch('https://project-artha-backend.stg.qume.io/api/user/kyc', {
      method: 'POST',
      headers: {
          "Content-type": "application/json"
      },
      body: JSON.stringify({
        "email": cachedEmail,
        "kycCode": kyc.code
      })
  })
  .then(function(response) {
    if(response.status === 204 || response.status === 409){
      modalShow1('You have Successfully completed KYC process');
    }else{
      modalShow1('Something Went Wrong. Please try later')
    }
  })
  .catch(function(error) {
    modalShow1('Something Went Wrong. Please try later')
    console.log("Error", error);
  });
  }

function modalShow1(text){
  var modal = document.getElementById('myModal1')
  document.getElementById('text-msg').innerHTML = text
  modal.style.display = 'block'
}
