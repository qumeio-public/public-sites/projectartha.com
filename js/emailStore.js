const handleClose = (event) => {
  event.target.parentNode.parentNode.parentNode.style.display = 'none'
}
//Email collection in start earning section
function storeEmail() {
  var field1 = $("#nameField").val();
  if(field1 == ""){
    alert('Please Enter Your Email');

    document.getElementById("nameField").focus();
    return false;
  }
  $.ajax({url: "https://docs.google.com/forms/d/e/1FAIpQLSfImayPsKIBAqzuxKvvi-_zynGj6gg-l7SQ4OdJU9HgSxR6hA/formResponse?",
    data: {"entry.1689629547": field1},
    type: "POST",
    dataType: "xml",
    success: function() {
        var modal = document.getElementById('myModal1')
        document.getElementById('text-msg').innerHTML = 'Your email is submitted successfully'
        modal.style.display = 'block'
    },
    error: function() {
      var modal = document.getElementById('myModal1')
      document.getElementById('text-msg').innerHTML = 'Error Occured. Try again later'
      modal.style.display = 'block'
    }
  });
  return false;
}
    
